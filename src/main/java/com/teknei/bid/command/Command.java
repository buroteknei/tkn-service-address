package com.teknei.bid.command;

/**
 * Interface designed for executing isolated flows regarding the invoker
 */
public interface Command {

    /**
     * Executes the command flow
     * @param request
     * @return the response to the flow execution
     */
    CommandResponse execute(CommandRequest request);

}