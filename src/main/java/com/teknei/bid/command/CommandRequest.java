package com.teknei.bid.command;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the request form to the flow
 */
@Data
public class CommandRequest implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(CommandRequest.class);
	
	    List<MultipartFile> files;
    List<byte[]> fileContent;
    Long id;
    String scanId;
    String documentId;
    RequestType requestType;
    String data;
    String curp;
    Status requestStatus;
    String username;

    public void setFiles(List<MultipartFile> files) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".setFiles ");
        this.files = files;
        List<byte[]> fContents = new ArrayList<>();
        files.forEach(f -> {
            try {
                fContents.add(f.getBytes());
            } catch (IOException e) {
                log.error("Error obtaining byte content from file {} with message: {}", f.getName(), e.getMessage());
            }
        });
        fileContent = fContents;
    }
}
