package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * The request made to the download service
 */
@Data
public class DocumentPictureRequestDTO implements Serializable {

    private String personalIdentificationNumber;
    private String id;
    private Boolean isAnverso;

}