package com.teknei.bid.service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class GVisionCaller {

    @Value("${tkn.address.ocr.key}")
    private String key;
	private static final Logger log = LoggerFactory.getLogger(GVisionCaller.class);
    public String validateCredentials(String request){
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".validateCredentials ");
        String url = new StringBuilder("https://vision.googleapis.com/v1/images:annotate?key=").append(key).toString();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(request, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
        if(responseEntity.getStatusCode().is2xxSuccessful()){
            return responseEntity.getBody();
        }
        return null;
    }

}
