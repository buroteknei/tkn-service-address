package com.teknei.bid;

import com.teknei.bid.controller.rest.AddressController;
import com.teknei.bid.service.AddressService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TknServiceAddressApplicationTests {

    @Autowired
	private AddressController addressController;
    @Autowired
    private AddressService addressService;

    private static final Logger log = LoggerFactory.getLogger(TknServiceAddressApplicationTests.class);


    @Test
    public void testName (){
        String test = "\n" +
                "Nombre y Domicilio\n" +
                "RES MIL SEISCIENTOS CINCUENTA Y CUATRO PESOS\n" +
                "THEUREL ROUSSELL GONZALO\n" +
                "RANGEL E COLIMA Y MICH HAB 5TA\n" +
                "A ESM Y POSTE 000001\n" +
                "PUEBLO NUEVO II LPZ\n" +
                "LA PAZ, BCS\n" +
                "Número de servicio";
        log.info("Name: {}", parseNameCFE(test));
    }

    //@Test
    //@Transactional
    public void testPreloadedClient(){
        addressService.syncWithPreloaded(10180l, "test");
    }


	//@Test
	public void contextLoads() throws JSONException {
	    String test = "AVISO RECIBO\\nTotal a pagar del periodo facturado\\n$136.00\\n(CIENTO TREINTA Y SEIS PESOS 00/100 M.N.)\\nComisión Federal de Electricidad\\nAv. Paseo de la Reforma Núm. 164, Col. Juárez, Ciudad de México C.P. 06600.\\nRFC: CSS160330CP7\\nCFE SUMINISTRADOR DE SERVICIOS BÁSICOS\\nNombre y Domicilio\\nMARTINEZ LAZARO FABIOLA\\nCALZ CAMARONES 99 EBD 502\\nEULALIA GUZMAN\\nNUEVA SANTA MARIA VMC. C.P. 02800\\nCIUDAD DE MEXICO, CDMX\\nNúmero de servicio\\n995 150 700 240\\nFecha límite de pago\\n24 NOV 2017\\nCuenta\\n09DM24D010950345\\nUso Tarifa Hilos\\nDoméstico\\n01\\nInformación importante\\nCorte a partir de 25 NOV 2017.\\nSu consumo de energía eléctrica está dentro\\ndel rango de consumo BÁSICO, que es\\nmenor a 150 kWh bimestrales.\\nNum. de\\nMedidor\\n073BPX\\nMedición de consumo\\nLectura\\nLectura\\nMult.\\nactual\\nanterior\\n01060\\n00912\\nConsumo\\nkWh\\n148\\nPromedio\\nDiario en $\\nPeríodo\\nPromedio\\nConsumo Días Diario en kWh\\n04 SEP 17 60 2.46\\nAL\\n03 NOV 17\\n2.26\\nApoyo gubernamental\\nCosto de producción\\nAportación Gubernamental\\nFacturación\\n$826.19\\n$708.83\\nConcepto\\nBásico\\nSuma\\nkWh\\n148\\n148\\nPrecio\\n0.793\\nSubtotal\\n117.36\\n117.36\\nGráfica de consumo en kWh\\nA mayor consumo de kWh menor Aportación Gubernamental.\\n858\\n280\\n500\\n148\\nAportación\\nkWh\\nImporte de la facturación\\nEnergía\\n117.36\\nIVA 16%\\n18.77\\nFac. del Periodo\\n136.13\\nDiferencia por redondeo\\n0.08\\nTotal\\n$136.21\\nLa gráfica tiene dos indicadores, el de abajo es tu consumo de\\nenergía y el de arriba es el porcentaje de la aportación\\ngubernamental aplicada a tu recibo\\nOMBO\\nCON LOS CORREOS QUE OFRECEN QUE\\nPAGUES LA LUZ EN LÍNEA CON UN DESCUENTO,\\ncre NUNCA OFRECE DESCUENTOS tiu 8, PACO IR TURCICI FARACAS\\nQUE NO TE SORPRENDAN\\nla página web\\nf CFENacional\\n@CFEmx\\ncfe.mx\\nFecha, hora y lugar de impresión: 08 NOV 17 08.26:26 hrs. Granjas 280 Libertad Azcapotzalco Ciudad de Mexico Mexico 02050\\nNúmero de servicio:995150700240\\n01 995150700240 171124 000000136 8\\nTotal a pagar:\\n-2888-\\nPORTE PAGADO\\nCARTAS\\nCA09-1597\\nAUTORIZADO POR SEPOMEX\\n(CIENTO TREINTA Y SEIS PESOS 00/100\\nCuenta: 09DM24DO10950345 Clave de envío: Repartir\\nM.N.)\\n$136.00\\";
	    JSONObject response = parseResponseCFE(test);
        log.info("Response: {}", response);

	}


    private JSONObject parseResponseCFE(String response) throws JSONException {
        String name = parseNameCFE(response);
        String dateCfe = parseDateCFE(response);
        String addressCfe = parseAddressCFE(response);
        String referenceCfe = parseReferenceCFE(response);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Nombre", name);
        jsonObject.put("TipoDocumento", "CFE");
        jsonObject.put("Fecha", dateCfe);
        jsonObject.put("Direccion", addressCfe);
        jsonObject.put("Referencia", referenceCfe);
        return jsonObject;
    }

    private String parseReferenceCFE(String response) {
        String regexPrimary = "\\d{3}\\s\\d{3}\\s\\d{3}\\s\\d{3}";
        String secondary = "\\d{3}\\s\\d{3}\\s\\d{3}";
        String fallback = "\\d{3}\\s\\d{3}";
        List<String> arrayRegex = Arrays.asList(regexPrimary, secondary, fallback);
        for (String s :
                arrayRegex) {
            Pattern pattern = Pattern.compile(s);
            Matcher matcher = pattern.matcher(response);
            int posBegin = 0;
            int posEnd = 0;
            if (matcher.find()) {
                posBegin = matcher.start();
                posEnd = matcher.end();
                response = response.substring(posBegin, posEnd);
                return response;
            }
        }
        return null;
    }

    private String parseAddressCFE(String response) {
        String tagBegin = parseNameCFE(response);
        String tagEndRegex = "\\d\\d\\s\\d\\d";
        if (tagBegin != null) {
            response = response.substring(response.indexOf(tagBegin));
            response = response.replace(tagBegin, "");
            response = response.trim();
            if (response.startsWith("\n") || response.startsWith("\\n")) {
                response = response.substring(2);
            }
            Pattern pattern = Pattern.compile(tagEndRegex);
            Matcher matcher = pattern.matcher(response);
            List<String> listAddressPars = new ArrayList<>();
            if (matcher.find()) {
                int pos = matcher.start();
                response = response.substring(0, pos);
                if(response.endsWith("\n") || response.endsWith("\\n")){
                    response = response.substring(0, response.length() - 3);
                }
                response = response.replace("\n", "|");
                response = response.replace("\\n", "|");
                String[] parsed = response.split("\\|");
                Pattern pattern1 = Pattern.compile("[[:lower:]]{3,}");
                Pattern pattern2 = Pattern.compile("\\d\\.\\d");
                for (String s :
                        parsed) {
                    Matcher matcher1 = pattern1.matcher(s);
                    Matcher matcher2 = pattern2.matcher(s);
                    if (!matcher1.find() && !matcher2.find()) {
                        listAddressPars.add(s);
                    }
                }
            }
            String lastSegment = listAddressPars.get(listAddressPars.size() - 1);
            if (lastSegment.contains(",")) {
                lastSegment.replace(",", "-");
                listAddressPars.remove(listAddressPars.size() - 1);
                listAddressPars.add(lastSegment);
            }
            StringBuilder stringBuilder = new StringBuilder();
            listAddressPars.forEach(a -> stringBuilder.append(a).append("-"));
            String address_1 = stringBuilder.toString();
            address_1 = address_1.substring(0, address_1.length() - 2);
            address_1 = address_1.replace("\n", "-");
            return address_1;
        }
        String tagEnd = "Número de servicio";
        //TODO inverse
        return null;
    }

    private String parseDateCFE(String response) {
        String regex = "\\d\\d\\s\\D\\D\\w\\s\\d\\d";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(response);
        int begin = 0;
        int end = 0;
        boolean found = true;
        String dateString = null;
        while (matcher.find()) {
            begin = matcher.start();
            end = matcher.end();
        }
        if (found) {
            dateString = response.substring(begin, end);
        } else {
            return null;
        }
        dateString.replaceAll("\n", "");
        dateString.replaceAll(" ", "-");
        return dateString;
    }


    private String parseNameCFE(String response) {
        String[] tokens = {"Nombre y Domicilio", "Nombre", "Domicio", "Nombrey", "ombre"};
        boolean foundToken = false;
        for (String t :
                tokens) {
            if (response.contains(t)) {
                response = response.substring(response.indexOf(t));
                foundToken = true;
                break;
            }
        }
        if (foundToken) {
            if (response.startsWith("\n") || response.startsWith("\\n")) {
                response = response.substring(2);
            }
            response = response.trim();

            int toEnd = response.indexOf("\n");
            if(toEnd == -1){
                toEnd = response.indexOf("\\n");
            }
            response = response.substring(toEnd);
            response = response.trim();
            if(response.startsWith("\n") || response.startsWith("\\n")){
                response = response.substring(2);
            }
            String[] numberWords = {"UNO", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE", "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE", "VEINTE", "TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA", "OCHENTA", "NOVENTA", "CIEN", "CIENTOS", "PESO", "Y"};
            int counter = 0;
            for (String s :
                    numberWords) {
                if(response.contains(s)){
                    counter++;
                }
                if(counter >= 3){
                    int pos = response.indexOf("\n");
                    if(pos == -1){
                        pos = response.indexOf("\\n");
                    }
                    response = response.substring(pos);
                    break;
                }
            }
            if (response.startsWith("\n") || response.startsWith("\\n")) {
                response = response.substring(2);
            }
            response = response.trim();
            int endIndex = response.indexOf("\n");
            if(endIndex == -1){
                endIndex = response.indexOf("\\n");
            }
            response = response.substring(0, endIndex);
            return response;
        } else {
            return null;
        }
    }

}
